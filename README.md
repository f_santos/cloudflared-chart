# Cloudflared Helm Chart

This Helm Chart runs a high availability Cloudflare tunnel, providing an alternative to external load balancers and k8s ingresses.

## Example

### Values

```yaml
---
tunnel:
  name: "mytunnelname"
  credentials:
    secret: "my-cloudflare-credentials"
  # https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/configuration/ingress
  ingress:
    - hostname: "my.domain.tld"
      service: http://myk8sservice:8080
    - service: http_status:404
```

### Secret

```yaml
---
# Cloudflared credentials
apiVersion: v1
kind: Secret
metadata:
  name: "my-cloudflare-credentials"
data:
  credentials.json: |
    {
      "AccountTag"   : "1234",
      "TunnelID"     : "1234",
      "TunnelName"   : "mytunnelname",
      "TunnelSecret" : "1234"
    }
```
