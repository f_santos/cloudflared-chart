{{/*
Expand the name of the chart.
*/}}
{{- define "cloudflared.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "cloudflared.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "cloudflared.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "cloudflared.labels" -}}
helm.sh/chart: {{ include "cloudflared.chart" . }}
{{ include "cloudflared.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "cloudflared.selectorLabels" -}}
app.kubernetes.io/name: {{ include "cloudflared.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Path
*/}}
{{- define "cloudflared.path" -}}
{{- print "/etc/cloudflared" -}}
{{- end }}

{{/*
Config path
*/}}
{{- define "cloudflared.configPath" -}}
{{- printf "%s/config" (include "cloudflared.path" . ) -}}
{{- end }}

{{/*
Config file
*/}}
{{- define "cloudflared.configFile" -}}
{{- print "config.yaml" -}}
{{- end }}

{{/*
Config file path
*/}}
{{- define "cloudflared.configFilePath" -}}
{{- printf "%s/%s" (include "cloudflared.configPath" . ) (include "cloudflared.configFile" . ) -}}
{{- end }}

{{/*
Credentials path
*/}}
{{- define "cloudflared.credsPath" -}}
{{- printf "%s/creds" (include "cloudflared.path" . ) -}}
{{- end }}

{{/*
Credentials file
*/}}
{{- define "cloudflared.credsFile" -}}
{{- print "credentials.json" -}}
{{- end }}

{{/*
Credentials file path
*/}}
{{- define "cloudflared.credsFilePath" -}}
{{- printf "%s/%s" (include "cloudflared.credsPath" . ) (include "cloudflared.credsFile" . ) -}}
{{- end }}

{{/*
Return true if a secret object should be created
*/}}
{{- define "cloudflared.createSecret" -}}
{{- if and .Values.tunnel.credentials.json (not .Values.tunnel.credentials.secret) }}
    {{- true -}}
{{- else -}}
{{- end -}}
{{- end -}}

{{/* 
Credentials Secret
*/}}
{{- define "cloudflared.secretName" -}}
{{- if .Values.tunnel.credentials.secret }}
    {{- printf "%s" (tpl .Values.tunnel.credentials.secret $) -}}
{{- else }}
    {{- printf "%s-tunnel-secret" (include "cloudflared.fullname" .) -}}
{{- end -}}
{{- end -}}
